<?php

/**
 * Theme functions
 */

/**
 * Preprocessing for classroom-courses-list-item.tpl.php.
 */
function classroom_preprocess_classroom_courses_list_item(&$variables) {
  $course = $variables['course'];
  $variables['nid'] = $course->nid;
  $variables['title'] = check_plain($course->title);
  $variables['start_t'] = $course->classroom_course['start_t'];
  $variables['end_t'] = $course->classroom_course['end_t'];
  $variables['course_links'] = classroom_course_links($course);
}

/**
 * Some of the next functions (theme_*) are almost identical and
 * maybe they must implemented in other way.
 */
function theme_classroom_course_add_teachers_form($form) {
  return _classroom_theme_course_users_form($form);
}

function theme_classroom_course_add_students_form($form) {
  return _classroom_theme_course_users_form($form);
}

function theme_classroom_course_remove_teachers_form($form) {
  return _classroom_theme_course_users_form($form);
}

function theme_classroom_course_remove_students_form($form) {
  return _classroom_theme_course_users_form($form);
}

function theme_classroom_course_add_resources_form($form) {
  return _classroom_theme_course_resources_form($form);
}

function theme_classroom_course_remove_resources_form($form) {
  return _classroom_theme_course_resources_form($form);
}

function theme_classroom_assignment_responses_form($form) {
  $headers = $form['#headers'];
  unset($form['#headers']);

  $rows = array();
  foreach ($form['responses'] as $name => $element) {
    if (!isset($element['grade'])) { continue; }
    $rows[] = array(
      drupal_render($element['name']),
      drupal_render($element['grade']),
      drupal_render($element['view']),
    );
    unset($form['responses'][$name]);
  }

  $output = theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;

}

/**
 * Theme function to show the users' form (students or teachers)
 * in a table.
 *
 * @param $form
 *   Form definition.
 * @return
 *   Form in HTML.
 */
function _classroom_theme_course_users_form($form) {
  if (isset($form['#headers'])) {
    $headers = $form['#headers'];
    unset($form['#headers']);
  } else {
    $headers = array();
  }

  $rows = array();
  foreach ($form['users'] as $name => $element) {
    if (!isset($element['uid'])) { continue; }
    $rows[] = array(
      drupal_render($element['uid']),
    );
    unset($form['users'][$name]);
  }

  $form['pager'] = array('#value' => theme('pager'));
  $output = theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme function to show the resources' form in a table.
 *
 * @param $form
 *   Form definition.
 * @return
 *   Form in HTML.
 */
function _classroom_theme_course_resources_form($form) {
  if (isset($form['#headers'])) {
    $headers = $form['#headers'];
    unset($form['#headers']);
  } else {
    $headers = array();
  }

  $rows = array();
  foreach ($form['resources'] as $name => $element) {
    if (!isset($element['nid'])) { continue; }
    $rows[] = array(
      drupal_render($element['nid']),
    );
    unset($form['resources'][$name]);
  }

  $output = theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;
}


