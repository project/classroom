<?php

/**
 * @file classroom-course-list-item.tpl.php
 * Display a course in the courses list.
 *
 * Variables available:
 * - $nid
 * - $title
 * - $start_t
 * - $end_t
 * - $links
 *
 * If you need anything else, the variable $course is also available.
 * - $course
 *
 * @see classroom_preprocess_classroom_course_list_item
 */
?>

<div class="classroom-course-item-list">
<a href="node/{$nid}"><?= $title ?></a><br/>

<strong><?= t("Start") ?>:</strong>
<?= format_date($start_t, 'small') ?><br/>

<strong><?php echo t("End") ?>:</strong>
<?= format_date($end_t, 'small') ?>

<?php if (!empty($course_links)): ?>
<br/>
<?php echo implode($course_links, " | ") ?>
<?php endif; ?>
</div>
