<?php

/**
 * @file classroom-students-list-item.tpl.php
 *
 * Variables available:
 * - $a_user
 * - $links
 *
 * @see classroom_preprocess_classroom_students_list_item
 */
?>
<li>
<?php echo l($a_user->name, "user/$a_user->uid") ?>
<?php if (!empty($links)): ?>
 (<?php echo implode(' | ', $links) ?>)
<?php endif ?>
</li>
