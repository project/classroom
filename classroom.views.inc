<?php 

/**
 *
 * Created by Will Jaspers, NuAire, Inc.
 * v1.2 - Removed excess filters/arguments/etc.
 *  Known Issues:
 *    Relationships are absolutely necessary between: 
 *      assignment and course,
 *      resource and course,
 *      and more.
 *    
 *
 * v1.1 - Exposed most data to views
 *	Known Issues: 
 *    Cannot filter on CLASSROOM_<ROLE>
 * 	  Ignored classroom_signup_request table until it is fixed up.
 *    Finding certain by type may require a 'Relationship' filter in Views.
 *    Base tables are exposed, although they may be ultimately unnecessary.
 *
 * v1.0 - Initial dev release 
 *
 */

function classroom_views_data()
{
  $data = array();
  
    
  // --------------------- COURSES ------------------ //
  
    
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_course']['table']['group'] = t('Classroom');
/**
  $data['classroom_course']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Classroom: Courses'),
    'help' => t("Courses within our classroom."),
  );
/**/
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_course']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_table' => 'node',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'role' => array(
      'left_table' => 'role',
      'left_field' => 'rid',
      'field' => 'access_by_rid',
    ),
  );

  $data['classroom_course']['nid'] = array(
    'real field' => 'nid',
    'title' => t('Course: ID'),
    'help' => t('The Course ID.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric',
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  
  $data['classroom_course']['start_t'] = array(
    'title' => t('Course: Start Date'),
    'help' => t('When class begins.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['classroom_course']['end_t'] = array(
    'title' => t('Course: End Date'),
    'help' => t('When class ends.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['classroom_course']['registration_start'] = array(
    'title' => t('Course: Registration Start'),
    'help' => t('When class is available for registration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['classroom_course']['registration_end'] = array(
    'title' => t('Course: Registration End Date'),
    'help' => t('When class is no longer available for registration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['classroom_course']['access_by_rid'] = array(
    'title' => t('Course: Role ID'),
    'help' => t('The role ID that may access this course.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Role ID'),
      'base' => 'role',
      'base field' => 'rid',
    ),
  );
  
  $data['classroom_course']['use_grade_percentages'] = array(
    'title' => t('Course: Use grade Percentages'),
    'help' => t('Whether or not this course weighs assignments by percentage.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Uses Grade Percentages'),
      'type' => 'yes-no',
    ),
  );
 
 
  // --------------------- TOPICS ------------------ //  
  
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_topic']['table']['group'] = t('Classroom');
 
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_topic']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'classroom_course' => array(
      'left_table' => 'classroom_courses',
      'left_field' => 'nid',
      'field' => 'course_nid',
    ),
  );
  
  $data['classroom_topic']['tid'] = array(
    'title' => t('Topic: ID'),
    'help' => t('The topic ID in a course.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title', 
      'numeric' => TRUE,
    ),
  );

  $data['classroom_topic']['course_nid'] = array(
    'title' => t('Topic: Course ID'),
    'help' => t('The Course ID associated with a topic.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'base' => 'classroom_course',
      'base field' => 'nid',
      'skip base' => array('classroom_topic'),
    ),
  );

  $data['classroom_topic']['name'] = array(
    'title' => t('Topic: Name'),
    'help' => t('The name of a topic in a course.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['classroom_topic']['description'] = array(
    'title' => t('Topic: Description'),
    'help' => t('The description of a topic in a course.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['classroom_topic']['number'] = array(
    'title' => t('Topic: Number'),
    'help' => t('The topic number in a course.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title', 
      'numeric' => TRUE,
    ),
  );
 
  
  // --------------------- ASSIGNMENTS ------------------ //  
    
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_assignment']['table']['group'] = t('Classroom');

  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_assignment']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'classroom_course' => array(
      'left_table' => 'classroom_course',
      'left_field' => 'nid',
      'field' => 'course_nid',
    ),
    'classroom_topic' => array(
      'left_table' => 'classroom_topic',
      'left_field' => 'tid',
      'field' => 'topic_tid',
    ),
  );

  $data['classroom_assignment']['nid'] = array(
    'title' => t('Assignment: ID'),
    'help' => t('The assignment ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title', 
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  $data['classroom_assignment']['course_nid'] = array(
    'title' => t('Assignment: Course ID'),
    'help' => t('The Course ID associated with an assignment.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'base' => 'classroom_course',
      'base field' => 'nid',
      'skip base' => array('classroom_assignment'),
    ),
  );

  $data['classroom_assignment']['end_t'] = array(
    'title' => t('Assignment: Due Date'),
    'help' => t('When an assigment must be completed.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['classroom_assignment']['relative_end'] = array(
    'title' => t('Assignment: Relative End'),
    'help' => t('Unknown field -- assumed date type.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  $data['classroom_assignment']['grade_percentage'] = array(
    'title' => t('Assignment: Grade Percentage'),
    'help' => t('The grade weight for this assignment against the entire course.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  $data['classroom_assignment']['max_grade'] = array(
    'title' => t('Assignment: Maximum Grade'),
    'help' => t('The highest grade possible for this assignment.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  $data['classroom_assignment']['topic_tid'] = array(
    'title' => t('Assignment: Topic ID'),
    'help' => t('The Topic ID associated with an assignment.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Topic ID'),
      'base' => 'classroom_topic',
      'base field' => 'tid',
      'skip base' => array('classroom_assignment'),
    ),
  );  

  
  // --------------------- RESPONSES ------------------ //  
  
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_response']['table']['group'] = t('Classroom');
 
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_response']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'classroom_course' => array(
      'left_table' => 'classroom_courses',
      'left_field' => 'nid',
      'field' => 'course_nid',
    ),
    'classroom_assignment' => array(
      'left_table' => 'classroom_assignment',
      'left_field' => 'nid',
      'field' => 'assignment_nid',
    ),
  );

  $data['classroom_response']['assignment_nid'] = array(
    'title' => t('Response: Assignment ID'),
    'help' => t('The assignment ID this response is related to.'),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Assignment ID'),
      'base' => 'classroom_assignment',
      'base field' => 'nid',
      'skip base' => array('classroom_response'),
    ),
  );

  $data['classroom_response']['grade'] = array(
    'title' => t('Response: Grade'),
    'help' => t('The grade given in response to an assignment.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  // --------------------- RESOURCES ------------------ //  
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_course_resource']['table']['group'] = t('Classroom');

  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_course_resource']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'resource_nid',
    ),
    'classroom_course' => array(
      'left_field' => 'tid',
      'field' => 'topic_tid',
    ),
    'classroom_topic' => array(
      'left_field' => 'tid',
      'field' => 'topic_tid',
    ),
  );

  $data['classroom_course_resource']['resource_nid'] = array(
    'title' => t('Resource: ID'),
    'help' => t('The Resource ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'parent' => 'views_handler_argument_numeric',
      'name field' => 'title', 
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  $data['classroom_course_resource']['course_nid'] = array(
    'title' => t('Resource: Course ID'),
    'help' => t('The Course ID associated with a resource.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'base' => 'classroom_course',
      'base field' => 'nid',
      'skip base' => array('classroom_resource'),
    ),
  );
  
  $data['classroom_course_resource']['topic_tid'] = array(  
    'title' => t('Resource: Topic ID'),
    'help' => t('The Topic ID associated with a resource.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Topic ID'),
      'base' => 'classroom_topic',
      'base field' => 'tid',
      'skip base' => array('classroom_resource'),
    ),
  );

  // --------------------- REGISTRATION ------------------ //  
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['classroom_registration']['table']['group'] = t('Classroom');
  
  $data['classroom_registration']['table']['base'] = array(
    'title' => t('Classroom: Course Registrations'),
    'help' => t("Users whom have registered for Courses."),
  );
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['classroom_registration']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'course_nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'role' => array(
      'left_field' => 'rid',
      'field' => 'access_by_rid',
    ),
  );

  $data['classroom_registration']['course_nid'] = array(
    'title' => t('Registration: Course ID'),
    'help' => t('The Course ID associated with a resource.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Course ID'),
      'base' => 'classroom_course',
      'base field' => 'nid',
      'skip base' => array('classroom_registration'),
    ),
  );

  $data['classroom_registration']['active'] = array(  
    'title' => t('Registration: Active'),
    'help' => t('Whether or not the users registration is accepted.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Active'),
      'type' => 'yes-no',
    ),
  );
  
  /**
   * IMPORTANT: 
   * Registration Roles are NOT the same as the built-in role table from Drupal.
   * See classroom.module for definitions of the values you will find here.
   * Look for: "CLASSROOM_STUDENT", "CLASSROOM_MANAGER", "CLASSROOM_TEACHER"
   */
  $data['classroom_registration']['role'] = array(  
    'title' => t('Registration: Role'),
    'help' => t('What persona the user is in relation to a course. (DEV: Needs Work)'),
    // TODO: Add Sorting,
    // TODO: Add Argument,
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  $data['classroom_registration']['date'] = array(
    'title' => t('Registration: Date'),
    'help' => t('When a user registered for class.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
